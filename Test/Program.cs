﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UndertaleModLib;
using UndertaleModLib.Decompiler;
using UndertaleModLib.Models;

namespace Test
{
    class Program
    {
        static void saveExtension(string filename, UndertaleExtension ext)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(filename, FileMode.Create)))
            {
                writer.Write(ext.Name.Content);
                writer.Write(ext.ClassName.Content);
                writer.Write(ext.EmptyString.Content);
                int filesLength = ext.Files.ToArray().Length;
                writer.Write(filesLength);
                for(int i = 0; i < filesLength; ++i)
                {
                    var file = ext.Files[i];
                    writer.Write(file.Filename.Content);
                    writer.Write((uint)file.Kind);
                    writer.Write(file.InitScript.Content);
                    writer.Write(file.CleanupScript.Content);
                    int functionsLength = file.Functions.ToArray().Length;
                    writer.Write(functionsLength);
                    for(int j = 0; j < functionsLength; ++j)
                    {
                        var fun = file.Functions[j];
                        writer.Write(fun.ID);
                        writer.Write(fun.Kind);
                        writer.Write(fun.Name.Content);
                        writer.Write((uint)fun.RetType);
                        writer.Write(fun.ExtName.Content);
                        int argumentsLength = fun.Arguments.ToArray().Length;
                        writer.Write(argumentsLength);
                        for(int k = 0; k < argumentsLength; ++k)
                        {
                            var arg = fun.Arguments[k];
                            writer.Write((uint)arg.Type);
                        }
                    }
                }
            }
        }
        static UndertaleExtension loadExtension(string filename, UndertaleData data)
        {
            var ext = new UndertaleExtension();
            using (BinaryReader reader = new BinaryReader(File.Open(filename, FileMode.Open)))
            {
                ext.Name = data.Strings.MakeString(reader.ReadString());
                ext.ClassName = data.Strings.MakeString(reader.ReadString());
                ext.EmptyString = data.Strings.MakeString(reader.ReadString());
                int filesLength = reader.ReadInt32();
                for(int i = 0; i < filesLength; ++i)
                {
                    var file = new UndertaleExtension.ExtensionFile();
                    file.Filename = data.Strings.MakeString(reader.ReadString());
                    file.Kind = (UndertaleExtension.ExtensionKind)reader.ReadUInt32();
                    file.InitScript = data.Strings.MakeString(reader.ReadString());
                    file.CleanupScript = data.Strings.MakeString(reader.ReadString());
                    int functionsLength = reader.ReadInt32();
                    for(int j = 0; j < functionsLength; ++j)
                    {
                        uint ID = reader.ReadUInt32();
                        uint Kind = reader.ReadUInt32();
                        string Name = reader.ReadString();
                        var RetType = (UndertaleExtension.ExtensionVarType)reader.ReadUInt32();
                        string ExtName = reader.ReadString();
                        int argumentsLength = reader.ReadInt32();
                        var args = new UndertaleExtension.ExtensionVarType[argumentsLength];
                        for (int k = 0; k < argumentsLength; ++k)
                        {
                            args[k] = (UndertaleExtension.ExtensionVarType)reader.ReadUInt32();
                        }
                        file.Functions.DefineExtensionFunction(data.Functions, data.Strings, ID, Kind, Name, RetType, ExtName, args);
                    }
                    ext.Files.Add(file);
                }
            }
            return ext;
        }
        static void saveSound(string filename, UndertaleSound sound)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(filename, FileMode.Create)))
            {
                writer.Write(sound.Name.Content);
                writer.Write(sound.Pitch);
                writer.Write(sound.GroupID);
                writer.Write(sound.Volume);
                writer.Write(sound.Type.Content);
                writer.Write(sound.File.Content);
                writer.Write(sound.Effects);
                writer.Write(sound.AudioGroup != null);
                if (sound.AudioGroup != null)
                {
                    writer.Write(sound.AudioGroup.Name.Content);
                }
                writer.Write((uint)sound.Flags);
                writer.Write(sound.AudioFile != null);
                if (sound.AudioFile != null)
                {
                    writer.Write(sound.AudioFile.Data.Length);
                    writer.Write(sound.AudioFile.Data);
                }
            }
        }
        static UndertaleSound loadSound(string filename, UndertaleData data)
        {
            var sound = new UndertaleSound();
            using (BinaryReader reader = new BinaryReader(File.Open(filename, FileMode.Open)))
            {
                sound.Name = data.Strings.MakeString(reader.ReadString());
                sound.Pitch = reader.ReadSingle();
                sound.GroupID = reader.ReadInt32();
                sound.Volume = reader.ReadSingle();
                sound.Type = data.Strings.MakeString(reader.ReadString());
                sound.File = data.Strings.MakeString(reader.ReadString());
                sound.Effects = reader.ReadUInt32();
                bool hasAudioGroup = reader.ReadBoolean();
                if (hasAudioGroup)
                {
                    sound.AudioGroup = new UndertaleAudioGroup();
                    sound.AudioGroup.Name = data.Strings.MakeString(reader.ReadString());
                    sound.GroupID = data.AudioGroups.ToArray().Length;
                    data.AudioGroups.Add(sound.AudioGroup);
                }
                sound.Flags = (UndertaleSound.AudioEntryFlags)reader.ReadUInt32();
                sound.AudioID = -1;
                bool hasAudioFile = reader.ReadBoolean();
                if (hasAudioFile)
                {
                    sound.AudioFile = new UndertaleEmbeddedAudio();
                    int bytesLength = reader.ReadInt32();
                    sound.AudioFile.Data = reader.ReadBytes(bytesLength);
                    sound.AudioID = data.EmbeddedAudio.ToArray().Length;
                    data.EmbeddedAudio.Add(sound.AudioFile);
                }
            }
            return sound;
        }
        static void Main(string[] args)
        {
            if(args.Length < 7)
            {
                Console.Error.Write("Invalid args");
                Console.Error.Write("Usage: converterGMS.exe input output gameName gameID address portTCP portUDP");
                Environment.Exit(1);
            }
            string input = args[0];
            string output = args[1];
            string gameName = args[2];
            string gameID = args[3];
            string address = args[4];
            string port_tcp = args[5];
            string port_udp = args[6];
            /*
            string input = "data2.win";
            string output = "data.win";
            string gameName = "micromedley";
            string gameID = "some id";
            string address = "isocodes.org";
            string port_tcp = "3003";
            string port_udp = "3005";
            */
            UndertaleData data = UndertaleIO.Read(new FileStream(input, FileMode.Open, FileAccess.Read));
            if(data.GameObjects.ByName("__ONLINE_onlinePlayer") != null)
            {
                Console.Error.Write("This game is already an online version");
                Environment.Exit(1);
            }
            // var DECOMPILE_CONTEXT = new DecompileContext(data, true);
            /// Extensions
            data.Extensions.Add(loadExtension("http_dll", data));
            /// Sounds
            data.Sounds.Add(loadSound("sound_chatbox", data));
            data.Sounds.Add(loadSound("sound_saved", data));
            /// Fonts
            /// TODO: Add the font here
            /// Objects
            var world = data.GameObjects.ByName("objWorld");
            if (world == null)
            {
                Console.Error.Write("Unable to find the object objWorld");
                Environment.Exit(1);
            }
            var player = data.GameObjects.ByName("objPlayer");
            if (player == null)
            {
                Console.Error.Write("Unable to find the object objPlayer");
                Environment.Exit(1);
            }
            var onlinePlayer = new UndertaleGameObject();
            onlinePlayer.Name = data.Strings.MakeString("__ONLINE_onlinePlayer");
            onlinePlayer.Visible = false;
            onlinePlayer.Persistent = true;
            onlinePlayer.Depth = -10;
            onlinePlayer.Awake = true;
            data.GameObjects.Add(onlinePlayer);
            var onlineChatbox = new UndertaleGameObject();
            onlineChatbox.Name = data.Strings.MakeString("__ONLINE_chatbox");
            onlineChatbox.Visible = true;
            onlineChatbox.Persistent = true;
            onlineChatbox.Depth = -11;
            onlineChatbox.Awake = true;
            data.GameObjects.Add(onlineChatbox);
            var playerSaved = new UndertaleGameObject();
            playerSaved.Name = data.Strings.MakeString("__ONLINE_playerSaved");
            playerSaved.Visible = true;
            playerSaved.Persistent = false;
            playerSaved.Depth = -10;
            playerSaved.Awake = true;
            data.GameObjects.Add(playerSaved);
            world.EventHandlerFor(EventType.Create, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
/// ONLINE
__ONLINE_connected = false;
__ONLINE_buffer = hbuffer_create();
__ONLINE_selfID = """";
__ONLINE_name = """";
__ONLINE_selfGameID = """ + gameID + @""";
__ONLINE_server = """ + address + @""";
__ONLINE_socket = socket_create();
socket_connect(__ONLINE_socket, __ONLINE_server, " + port_tcp + @");
__ONLINE_name = get_string(""Enter your name:"", """");
if(__ONLINE_name == """"){
__ONLINE_name = ""Anonymous"";
}
__ONLINE_name = string_replace_all(__ONLINE_name, ""#"", ""\#"");
if(string_length(__ONLINE_name) > 20){
__ONLINE_name = string_copy(__ONLINE_name, 0, 20);
}
__ONLINE_password = get_string(""Password (leave it empty for no password):"", """");
if(string_length(__ONLINE_password) > 20){
__ONLINE_password = string_copy(__ONLINE_password, 0, 20);
}
__ONLINE_selfGameID += __ONLINE_password;
hbuffer_clear(__ONLINE_buffer);
hbuffer_write_uint8(__ONLINE_buffer, 3);
hbuffer_write_string(__ONLINE_buffer, __ONLINE_name);
hbuffer_write_string(__ONLINE_buffer, __ONLINE_selfGameID);
hbuffer_write_string(__ONLINE_buffer, """ + gameName + @""");
socket_write_message(__ONLINE_socket, __ONLINE_buffer);
__ONLINE_udpsocket = udpsocket_create();
udpsocket_start(__ONLINE_udpsocket, false, 0);
udpsocket_set_destination(__ONLINE_udpsocket, __ONLINE_server, " + port_udp + @");
hbuffer_clear(__ONLINE_buffer);
hbuffer_write_uint8(__ONLINE_buffer, 0);
udpsocket_send(__ONLINE_udpsocket, __ONLINE_buffer);
__ONLINE_pExists = false;
__ONLINE_pX = 0;
__ONLINE_pY = 0;
__ONLINE_t = 0;
__ONLINE_sGravity = 0;
__ONLINE_sX = 0;
__ONLINE_sY = 0;
__ONLINE_sRoom = 0;
__ONLINE_sSaved = false;
            ", data);
            world.EventHandlerFor(EventType.Step, EventSubtypeStep.EndStep, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
/// ONLINE
/// TCP SOCKETS
socket_update_read(__ONLINE_socket);
while(socket_read_message(__ONLINE_socket, __ONLINE_buffer)){
switch(hbuffer_read_uint8(__ONLINE_buffer)){
case 0:
// CREATED
__ONLINE_ID = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_found = false;
for(__ONLINE_i = 0; __ONLINE_i < instance_number(__ONLINE_onlinePlayer) && !__ONLINE_found; __ONLINE_i += 1){
if(instance_find(__ONLINE_onlinePlayer, __ONLINE_i).__ONLINE_ID == __ONLINE_ID){
__ONLINE_found = true;
}
}
if(!__ONLINE_found){
__ONLINE_oPlayer = instance_create(0, 0, __ONLINE_onlinePlayer);
__ONLINE_oPlayer.__ONLINE_ID = __ONLINE_ID;
__ONLINE_oPlayer.__ONLINE_name = hbuffer_read_string(__ONLINE_buffer);;
}
break;
case 1:
// DESTROYED
__ONLINE_ID = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_found = false;
for(__ONLINE_i = 0; __ONLINE_i < instance_number(__ONLINE_onlinePlayer) && !__ONLINE_found; __ONLINE_i += 1){
__ONLINE_oPlayer = instance_find(__ONLINE_onlinePlayer, __ONLINE_i);
if(__ONLINE_oPlayer.__ONLINE_ID == __ONLINE_ID){
with(__ONLINE_oPlayer){
instance_destroy();
}
__ONLINE_found = true;
}
}
break;
case 4:
// CHAT MESSAGE
__ONLINE_ID = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_found = false;
__ONLINE_oPlayer = 0;
for(__ONLINE_i = 0; __ONLINE_i < instance_number(__ONLINE_onlinePlayer) && !__ONLINE_found; __ONLINE_i += 1){
__ONLINE_oPlayer = instance_find(__ONLINE_onlinePlayer, __ONLINE_i);
if(__ONLINE_oPlayer.__ONLINE_ID == __ONLINE_ID){
__ONLINE_found = true;
}
}
if(__ONLINE_found){
__ONLINE_message = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_oChatbox = instance_create(0, 0, __ONLINE_chatbox);
__ONLINE_oChatbox.__ONLINE_message = __ONLINE_message;
__ONLINE_oChatbox.__ONLINE_follower = __ONLINE_oPlayer;
if(__ONLINE_oPlayer.visible){
audio_play_sound(__ONLINE_sndChatbox, 0, false);
}
}
break;
case 5:
// SOMEONE SAVED
__ONLINE_sSaved = true;
__ONLINE_sGravity = hbuffer_read_uint8(__ONLINE_buffer);
__ONLINE_sName = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_sX = hbuffer_read_int32(__ONLINE_buffer);
__ONLINE_sY = hbuffer_read_float64(__ONLINE_buffer);
__ONLINE_sRoom = hbuffer_read_int16(__ONLINE_buffer);
__ONLINE_a = instance_create(0, 0, __ONLINE_playerSaved);
__ONLINE_a.__ONLINE_name = __ONLINE_sName;
audio_play_sound(__ONLINE_sndSaved, 0, false);
break;
case 6:
// SELF ID
__ONLINE_selfID = hbuffer_read_string(__ONLINE_buffer);
}
}
__ONLINE_mustQuit = false;
switch(socket_get_state(__ONLINE_socket)){
case 2:
if(!__ONLINE_connected){
__ONLINE_connected = true;
}
break;
case 4:
show_message(""Connection closed."");
__ONLINE_mustQuit = true;
break;
case 5:
socket_reset(__ONLINE_socket);
if(__ONLINE_connected){
show_message(""Connection lost."");
}else{
show_message(""Could not connect to the server."");
}
__ONLINE_mustQuit = true;
break;
}
if(__ONLINE_mustQuit){
game_end();
}
__ONLINE_p = objPlayer;
__ONLINE_exists = instance_exists(__ONLINE_p);
__ONLINE_X = __ONLINE_pX;
__ONLINE_Y = __ONLINE_pY;
if(__ONLINE_exists){
if(__ONLINE_exists != __ONLINE_pExists){
// SEND PLAYER CREATE
hbuffer_clear(__ONLINE_buffer);
hbuffer_write_uint8(__ONLINE_buffer, 0);
socket_write_message(__ONLINE_socket, __ONLINE_buffer);
}
__ONLINE_X = __ONLINE_p.x;
__ONLINE_Y = __ONLINE_p.y;
if(__ONLINE_pX != __ONLINE_X || __ONLINE_pY != __ONLINE_Y || __ONLINE_t < 3){
if(__ONLINE_t >= 3){
__ONLINE_t = 0;
}
// SEND PLAYER MOVED
if(__ONLINE_selfID != """"){
hbuffer_clear(__ONLINE_buffer);
hbuffer_write_uint8(__ONLINE_buffer, 1);
hbuffer_write_string(__ONLINE_buffer, __ONLINE_selfID);
hbuffer_write_string(__ONLINE_buffer, __ONLINE_selfGameID);
hbuffer_write_uint16(__ONLINE_buffer, room);
hbuffer_write_int32(__ONLINE_buffer, __ONLINE_X);
hbuffer_write_int32(__ONLINE_buffer, __ONLINE_Y);
hbuffer_write_int32(__ONLINE_buffer, __ONLINE_p.sprite_index);
hbuffer_write_float32(__ONLINE_buffer, __ONLINE_p.image_speed);
hbuffer_write_float32(__ONLINE_buffer, __ONLINE_p.image_xscale*" + (data.Variables.Where((x) => x.Name.Content == "player_xscale").FirstOrDefault() == null ? "__ONLINE_p.xScale" : "global.player_xscale") + @");
hbuffer_write_float32(__ONLINE_buffer, __ONLINE_p.image_yscale*global.grav);
hbuffer_write_float32(__ONLINE_buffer, __ONLINE_p.image_angle);
hbuffer_write_string(__ONLINE_buffer, __ONLINE_name);
udpsocket_send(__ONLINE_udpsocket, __ONLINE_buffer);
}
}
__ONLINE_t += 1;
if(keyboard_check_pressed(vk_space)){
__ONLINE_message = get_string(""Say something:"", """");
__ONLINE_message = string_replace_all(__ONLINE_message, ""#"", ""\#"");
__ONLINE_message_length = string_length(__ONLINE_message);
if(__ONLINE_message_length > 0){
__ONLINE_message_max_length = 300;
if(__ONLINE_message_length > __ONLINE_message_max_length){
__ONLINE_message = string_copy(__ONLINE_message, 0, __ONLINE_message_max_length);
}
hbuffer_clear(__ONLINE_buffer);
hbuffer_write_uint8(__ONLINE_buffer, 4);
hbuffer_write_string(__ONLINE_buffer, __ONLINE_message);
socket_write_message(__ONLINE_socket, __ONLINE_buffer);
__ONLINE_oChatbox = instance_create(0, 0, __ONLINE_chatbox);
__ONLINE_oChatbox.__ONLINE_message = __ONLINE_message;
__ONLINE_oChatbox.__ONLINE_follower = __ONLINE_p;
audio_play_sound(__ONLINE_sndChatbox, 0, false);
}
}
}else{
if(__ONLINE_exists != __ONLINE_pExists){
// SEND PLAYER DESTROYED
hbuffer_clear(__ONLINE_buffer);
hbuffer_write_uint8(__ONLINE_buffer, 1);
socket_write_message(__ONLINE_socket, __ONLINE_buffer);
}
}
__ONLINE_pExists = __ONLINE_exists;
__ONLINE_pX = __ONLINE_X;
__ONLINE_pY = __ONLINE_Y;
socket_update_write(__ONLINE_socket);
/// UDP SOCKETS
while(udpsocket_receive(__ONLINE_udpsocket, __ONLINE_buffer)){
switch(hbuffer_read_uint8(__ONLINE_buffer)){
case 1:
// RECEIVE MOVED
__ONLINE_ID = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_gameID = hbuffer_read_string(__ONLINE_buffer);
__ONLINE_found = false;
__ONLINE_oPlayer = 0;
for(__ONLINE_i = 0; __ONLINE_i < instance_number(__ONLINE_onlinePlayer) && !__ONLINE_found; __ONLINE_i += 1){
__ONLINE_oPlayer = instance_find(__ONLINE_onlinePlayer, __ONLINE_i);
if(__ONLINE_oPlayer.__ONLINE_ID == __ONLINE_ID){
__ONLINE_found = true;
}
}
if(!__ONLINE_found){
__ONLINE_oPlayer = instance_create(0, 0, __ONLINE_onlinePlayer);
__ONLINE_oPlayer.__ONLINE_ID = __ONLINE_ID;
}
__ONLINE_oPlayer.__ONLINE_oRoom = hbuffer_read_uint16(__ONLINE_buffer);
__ONLINE_oPlayer.x = hbuffer_read_int32(__ONLINE_buffer);
__ONLINE_oPlayer.y = hbuffer_read_int32(__ONLINE_buffer);
__ONLINE_oPlayer.sprite_index = hbuffer_read_int32(__ONLINE_buffer);
__ONLINE_oPlayer.image_speed = hbuffer_read_float32(__ONLINE_buffer);
__ONLINE_oPlayer.image_xscale = hbuffer_read_float32(__ONLINE_buffer);
__ONLINE_oPlayer.image_yscale = hbuffer_read_float32(__ONLINE_buffer);
__ONLINE_oPlayer.image_angle = hbuffer_read_float32(__ONLINE_buffer);
__ONLINE_oPlayer.__ONLINE_name = hbuffer_read_string(__ONLINE_buffer);
break;
default:
show_message(""Received unexpected data from the server."");
}
}
if(udpsocket_get_state(__ONLINE_udpsocket) != 1){
show_message(""Connection to the UDP socket lost."");
game_end();
}
            ", data);
            world.EventHandlerFor(EventType.Other, EventSubtypeOther.GameEnd, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
hbuffer_destroy(__ONLINE_buffer);
socket_destroy(__ONLINE_socket);
udpsocket_destroy(__ONLINE_udpsocket);
            ", data);
            onlinePlayer.EventHandlerFor(EventType.Create, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
__ONLINE_alpha = 1;
__ONLINE_oRoom = -1;
__ONLINE_name = """";
            ", data);
            onlinePlayer.EventHandlerFor(EventType.Step, EventSubtypeStep.EndStep, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
visible = __ONLINE_oRoom == room;
image_alpha = __ONLINE_alpha;
__ONLINE_p = objPlayer;
if(instance_exists(__ONLINE_p)){
__ONLINE_dist = distance_to_object(objPlayer);
image_alpha = min(__ONLINE_alpha, __ONLINE_dist/100);
}
            ", data);
            onlinePlayer.EventHandlerFor(EventType.Draw, EventSubtypeDraw.Draw, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
if(sprite_exists(sprite_index)){
draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_white, image_alpha);
__ONLINE__alpha = draw_get_alpha();
__ONLINE__color = draw_get_color();
draw_set_alpha(image_alpha);
if(font_exists(3)){
draw_set_font(3);
}
draw_set_valign(fa_center);
draw_set_halign(fa_center);
draw_set_color(c_black);
__ONLINE_border = 2;
__ONLINE_padding = 30;
__ONLINE_xx = x;
__ONLINE_yy = y-__ONLINE_padding;
draw_set_alpha(1);
draw_text(__ONLINE_xx+__ONLINE_border, __ONLINE_yy, __ONLINE_name);
draw_text(__ONLINE_xx, __ONLINE_yy+__ONLINE_border, __ONLINE_name);
draw_text(__ONLINE_xx-__ONLINE_border, __ONLINE_yy, __ONLINE_name);
draw_text(__ONLINE_xx, __ONLINE_yy-__ONLINE_border, __ONLINE_name);
draw_set_color(c_white);
draw_text(__ONLINE_xx, __ONLINE_yy, __ONLINE_name);
draw_set_alpha(__ONLINE__alpha);
draw_set_color(__ONLINE__color);
}
            ", data);
            onlineChatbox.EventHandlerFor(EventType.Create, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
__ONLINE_paddingText = 10;
__ONLINE_width = 250;
__ONLINE_padding = 15;
__ONLINE_alpha = 1;
__ONLINE_fade = false;
__ONLINE_fadeAlpha = 1;
__ONLINE_t = 70;
__ONLINE_sep = -1;
__ONLINE_maxTextWidth = __ONLINE_width-2*__ONLINE_paddingText;
__ONLINE_hasDestroyed = false;
            ", data);
            onlineChatbox.EventHandlerFor(EventType.Step, EventSubtypeStep.EndStep, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
__ONLINE_f = __ONLINE_follower;
if(instance_exists(__ONLINE_f)){
x = __ONLINE_f.x;
y = __ONLINE_f.y;
}else{
instance_destroy();
exit;
}
if(__ONLINE_fade){
__ONLINE_fadeAlpha -= 0.02;
if(__ONLINE_fadeAlpha <= 0){
instance_destroy();
exit;
}
}
__ONLINE_alpha = 1;
if(__ONLINE_follower != objPlayer){
visible = __ONLINE_follower.visible;
__ONLINE_p = objPlayer;
if(instance_exists(__ONLINE_p)){
__ONLINE_dist = distance_to_object(objPlayer);
__ONLINE_alpha = __ONLINE_dist/100;
}
}
__ONLINE_t -= 1;
if(__ONLINE_t < 0){
__ONLINE_fade = true;
}
if(!__ONLINE_hasDestroyed){
__ONLINE_found = false;
__ONLINE_oChatbox = 0;
for(__ONLINE_i = 0; __ONLINE_i < instance_number(__ONLINE_chatbox) && !__ONLINE_found; __ONLINE_i += 1){
__ONLINE_oChatbox = instance_find(__ONLINE_chatbox, __ONLINE_i);
if(__ONLINE_oChatbox.__ONLINE_follower == __ONLINE_follower && __ONLINE_oChatbox.id != id){
__ONLINE_found = true;
}
}
if(__ONLINE_found){
with(__ONLINE_oChatbox){
instance_destroy();
}
}
__ONLINE_hasDestroyed = true;
}
            ", data);
            onlineChatbox.EventHandlerFor(EventType.Draw, EventSubtypeDraw.Draw, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
__ONLINE_textHeight = string_height_ext(__ONLINE_message, __ONLINE_sep, __ONLINE_maxTextWidth);
__ONLINE_height = __ONLINE_textHeight+2*__ONLINE_paddingText;
__ONLINE_yOffset = -__ONLINE_height/2+60;
__ONLINE_left = 0;
__ONLINE_right = room_width;
__ONLINE_top = 0;
__ONLINE_bottom = room_height;
if(view_enabled && view_visible[0]){
__ONLINE_left = view_xview[0];
__ONLINE_right = __ONLINE_left+view_wview[0];
__ONLINE_top = view_yview[0];
__ONLINE_bottom = __ONLINE_top+view_hview[0];
}
__ONLINE_xx = min(max(x, __ONLINE_left+__ONLINE_width/2+__ONLINE_padding), __ONLINE_right-__ONLINE_width/2-__ONLINE_padding);
__ONLINE_yy = min(max(y-__ONLINE_yOffset, __ONLINE_top+__ONLINE_height/2+__ONLINE_padding), __ONLINE_bottom-__ONLINE_height/2-__ONLINE_padding);
__ONLINE__alpha = draw_get_alpha();
__ONLINE__color = draw_get_color();
draw_set_alpha(min(__ONLINE_alpha, __ONLINE_fadeAlpha));
draw_set_color(c_white);
draw_rectangle(__ONLINE_xx-__ONLINE_width/2, __ONLINE_yy-__ONLINE_height/2, __ONLINE_xx+__ONLINE_width/2, __ONLINE_yy+__ONLINE_height/2, false);
draw_set_color(c_black);
draw_rectangle(__ONLINE_xx-__ONLINE_width/2, __ONLINE_yy-__ONLINE_height/2, __ONLINE_xx+__ONLINE_width/2, __ONLINE_yy+__ONLINE_height/2, true);
if(font_exists(3)){
draw_set_font(3);
}
draw_set_valign(fa_center);
draw_set_halign(fa_center);
draw_text_ext(__ONLINE_xx, __ONLINE_yy, __ONLINE_message, __ONLINE_sep, __ONLINE_maxTextWidth);
draw_set_alpha(__ONLINE__alpha);
draw_set_color(__ONLINE__color);
            ", data);
            playerSaved.EventHandlerFor(EventType.Step, EventSubtypeStep.EndStep, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
image_alpha -= 0.01;
if(image_alpha <= 0){
instance_destroy();
}
            ", data);
            playerSaved.EventHandlerFor(EventType.Draw, EventSubtypeDraw.Draw, data.Strings, data.Code, data.CodeLocals).AppendGML(@"
__ONLINE_xx = 20;
__ONLINE_yy = 20;
if(view_enabled && view_visible[0]){
__ONLINE_xx += view_xview[0];
__ONLINE_yy += view_yview[0];
}
__ONLINE_text = __ONLINE_name+"" saved!"";
__ONLINE__alpha = draw_get_alpha();
__ONLINE__color = draw_get_color();
draw_set_valign(fa_top);
draw_set_halign(fa_left);
draw_set_alpha(image_alpha);
if(font_exists(3)){
draw_set_font(3);
}
draw_set_color(c_black);
draw_text(__ONLINE_xx + 1, __ONLINE_yy, __ONLINE_text);
draw_text(__ONLINE_xx, __ONLINE_yy + 1, __ONLINE_text);
draw_text(__ONLINE_xx - 1, __ONLINE_yy, __ONLINE_text);
draw_text(__ONLINE_xx, __ONLINE_yy - 1, __ONLINE_text);
draw_set_color(c_white);
draw_text(__ONLINE_xx, __ONLINE_yy, __ONLINE_text);
draw_set_alpha(__ONLINE__alpha);
draw_set_color(__ONLINE__color);
            ", data);
            /// Scripts
            var saveGame = data.Scripts.ByName("scrSaveGame");
            var loadGame = data.Scripts.ByName("scrLoadGame");
            if (saveGame == null)
            {
                Console.Error.Write("Unable to find the script scrSaveGame");
                Environment.Exit(1);
            }
            if (loadGame == null)
            {
                Console.Error.Write("Unable to find the script scrLoadGame");
                Environment.Exit(1);
            }
            saveGame.Code.AppendGML(@"
if(argument0){
hbuffer_clear(objWorld.__ONLINE_buffer);
__ONLINE_p = objPlayer;
if(instance_exists(__ONLINE_p)){
hbuffer_write_uint8(objWorld.__ONLINE_buffer, 5);
hbuffer_write_uint8(objWorld.__ONLINE_buffer, global.grav);
hbuffer_write_int32(objWorld.__ONLINE_buffer, __ONLINE_p.x);
hbuffer_write_float64(objWorld.__ONLINE_buffer, __ONLINE_p.y);
hbuffer_write_int16(objWorld.__ONLINE_buffer, room);
socket_write_message(objWorld.__ONLINE_socket, objWorld.__ONLINE_buffer);
}
}
            ", data);
            loadGame.Code.AppendGML(@"
if(objWorld.__ONLINE_sSaved){
if(room_exists(objWorld.__ONLINE_sRoom)){
if (global.grav != objWorld.__ONLINE_sGravity){
" + (data.Scripts.ByName("scrFlipGrav") != null ? "scrFlipGrav();" : "with(objPlayer){event_user(0);}") + @"
}
objPlayer.x = objWorld.__ONLINE_sX;
objPlayer.y = objWorld.__ONLINE_sY;
room_goto(objWorld.__ONLINE_sRoom);
}
objWorld.__ONLINE_sSaved = false;
}
            ", data);
            /// Recompile
            UndertaleIO.Write(new FileStream(output, FileMode.Create), data);
            Console.WriteLine("Success!");
        }
    }
}
